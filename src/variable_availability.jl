using DataFramesMeta
using DelimitedFiles
using CSV

header = vec(readdlm("src/header.csv", '\t', String))

df1 = CSV.read("src/df1.csv", DataFrame)
df2 = CSV.read("src/df2.csv", DataFrame)
df3 = CSV.read("src/df3.csv", DataFrame)

df_var_avail = @chain begin
    vcat(df1, df2, df3)
    rename!(header; makeunique = true)
    select(Not("Type"))
    select(Not(r"Variable[0-9].*"))
    select(Not("Variable Label"))
    rename("Variable" => "variable")
    stack(Not("variable"); variable_name = :country_year_short)
    @rtransform :country_short = strip(replace(:country_year_short, r"[0-9]" => ""))
    @rtransform :year = tryparse(Int, last(:country_year_short, 4))
    @transform :year = replace(:year, nothing => missing)
    @rtransform :value = :value == "X"
end

country_crosswalk = CSV.read("src/country_crosswalk.csv", DataFrame)

df_codes = @chain begin
    readdlm("src/country_codes.csv", '\t', String)
    DataFrame([:code, :country_year])
    @rtransform :country = strip(replace(:country_year, r"[0-9]" => ""))
    @rtransform :year = tryparse(Int, last(:country_year, 4))
    @transform :year = replace(:year, nothing => missing)
    leftjoin(country_crosswalk, on = :country)
    @select :code :country_short :year
end

avail = @chain begin
    leftjoin(df_var_avail, df_codes; on = [:country_short, :year], matchmissing = :notequal)
    select(:code, :variable, :value)
    dropmissing(:code)
    groupby(:code)
    begin
        x = map(keys(_)) do k
            str_k = k.code
            sdf = _[k]
            vars_contained = sdf.variable[sdf.value]
            str_k => vars_contained
        end
        Dict(x)
    end
end

open("src/var_avail.txt", "w+") do f
    for (k, v) in avail
        s = join([k; v], '\t')
        println(f, s)
    end
end

