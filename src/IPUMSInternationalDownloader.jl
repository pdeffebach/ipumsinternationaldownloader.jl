module IPUMSInternationalDownloader

using JSON3
using HTTP
using Downloads
using DelimitedFiles
using ProgressMeter
using Dates

export initiate_data_compilation, check_status, download_if_done,
    download_country_year,
    IPUMS_API_KEY, country_codes, var_avail


function __init__()
    global IPUMS_API_KEY = ENV["IPUMS_API_KEY"]

    codes_mat = readdlm(joinpath(@__DIR__, "country_codes.csv"), '\t')
    global country_codes = Dict(string.(codes_mat[:, 2]) .=> string.(codes_mat[:, 1]))

    global var_avail = Dict{String, Vector{String}}()
    for line in eachline(joinpath(@__DIR__, "var_avail.txt"))
           v = split(line, '\t')
           code = first(v)
           var_avail[code] = v[2:end]
    end
end

function initiate_data_compilation(code, description, varlist)

    headers = Dict(
        "Authorization" => IPUMS_API_KEY,
        "Content-Type" => "application/json"
    )

    query = Dict(
        "collection" => "ipumsi",
        "version" => 2
    )

    url = "https://api.ipums.org/extracts"

    avail_vars = var_avail[code]
    vars = intersect(varlist, avail_vars)

    body = Dict(
        "description" => description,
        "dataStructure" => Dict(
            Dict(
            "rectangular" =>
                Dict(
                    "on" => "P"))),
        "dataFormat" => "stata",
        "samples" => Dict(
            code => Dict()),
        "variables" => Dict(var => Dict() for var in vars)
    ) |> JSON3.write

    res = HTTP.request(
        "POST",
        url,
        headers,
        body;
        status_exception = false,
        query = query
    )

    return JSON3.read(String(copy(res.body)))
end

function check_status(m)
    extract_num = m["number"]

    headers = Dict(
        "Authorization" => IPUMS_API_KEY,
        "Content-Type" => "application/json"
    )

    query = Dict(
        "collection" => "ipumsi",
        "version" => 2
    )

    url = "https://api.ipums.org/extracts" * "/$extract_num"

    res = HTTP.request(
        "GET",
        url,
        headers;
        status_exception = false,
        query = query)

    return JSON3.read(String(copy(res.body)))
end

function download_if_done(m, outfolder)
    if m["status"] != "completed"
        error("Not ready for downloading")
    end

    url = m["downloadLinks"]["data"]["url"]

    outname = replace(
        replace(
            string(m["extractDefinition"]["description"], ".dta.gz"),
            r"\s" => ""),
        r"[\,\?\&]" => "")

    output = joinpath(outfolder, outname)

    headers = Dict(
        "Authorization" => IPUMS_API_KEY)

    p = Progress(0)
    p_fun = let p = p
        (total, now) -> begin
            if total != p.n
                p.n = total
            end
            update!(p, now)
        end
    end

    res = Downloads.download(
        url, output;
        method = "GET",
        headers = headers)
end

function download_country_year(code, varlist, description, outfolder)
    println("$(description): Intializing request")

    m_init = initiate_data_compilation(code, description, varlist)

    if !haskey(m_init, "number")
        error("$(description): Initialization unsuccessful")
    end
    extract_num = m_init["number"]
    println("$(description): Successfully initialized. Extract number $extract_num")
    start_time = Dates.now()
    total_time = 0
    width = 0
    while total_time < 7200
        sleep(30)
        total_time = total_time + 30

        elapsed = Dates.now() - start_time
        x = Dates.canonicalize(Dates.CompoundPeriod(elapsed))

        s = "$(description): Elapsed: $x"
        println(s)

        m_status = check_status(m_init)

        if m_status["status"] == "completed"
            println()
            println("$(description): Initializing download")
            download_if_done(m_status, outfolder)
            break
        end
    end
end

end
